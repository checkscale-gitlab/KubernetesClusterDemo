# Kubernetes Cluster Demo

----

### **Software requirements:**
* Terminal (for Windows e.g. [Git Bash](https://git-scm.com/downloads))
* [Docker](https://docs.docker.com/get-docker/)
* [Kubernetes](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [VS Code](https://code.visualstudio.com/download) (optional - for development and tests)
* [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) - VS Code extension (optional - for tests)
* [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) - VS Code extension pack (optional - for local development without installing needed tools)

----

### **Documentation:**
Documentation is available [here](http://localhost/api/swagger) after deploy Kubernetes services.

### **API:**
| HTTP Method | URL | Description |
|:---|:---|:---|
| `GET` | [`/api/healthcheck`](src/DotNetApi/Controllers/HealthcheckController.cs) | Base method for checking service's status. Return [`BaseResponse`](src/DotNetApi/Models/Bases/BaseResponse.cs) model. |