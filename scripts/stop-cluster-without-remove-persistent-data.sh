#!/bin/sh

kubectl delete -n=kubernetes-cluster-demo --all statefulset
kubectl delete -n=kubernetes-cluster-demo --all ing
kubectl delete -n=kubernetes-cluster-demo --all svc
kubectl delete -n=kubernetes-cluster-demo --all deploy
kubectl delete -n=kubernetes-cluster-demo --all cm
kubectl delete -n=kubernetes-cluster-demo --all rolebinding
kubectl delete -n=kubernetes-cluster-demo --all role
kubectl delete -n=kubernetes-cluster-demo --all sa