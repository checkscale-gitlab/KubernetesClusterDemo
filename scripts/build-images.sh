#!/bin/sh

cd ../src/DotNetApi
docker build -t dotnet-api-image .
cd -

cd ../src/NodePublisher
docker build -t node-publisher-image .
cd -