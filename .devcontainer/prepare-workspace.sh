#!/bin/sh

function configureRedis() {
    sed -i -e 's/supervised no/supervised systemd/g' /etc/redis.conf
}

function installPackages() {
    packages=$(cat alpine.packages | tr '\n' ' ')
    testingRepositoryUrl="http://dl-cdn.alpinelinux.org/alpine/edge/testing"

    echo $testingRepositoryUrl >> /etc/apk/repositories &&\
    apk add --no-cache --update $packages &&\
    rm -rf /var/cache/apk/*
}

function installDotnet() {
    dotnetApkUrl="https://download.visualstudio.microsoft.com/download/pr/1ea8c954-015d-4ded-a221-6bcc27f53d06/c76bcb58b9a1539dcba34c0cb6c5df9b/dotnet-sdk-5.0.100-rc.2.20479.15-linux-musl-x64.tar.gz"

    mkdir $HOME/dotnet &&\
    curl $dotnetApkUrl -o dotnet.tar.gz &&\
    tar -zxf dotnet.tar.gz -C /usr/bin/ &&\
    rm -f dotnet.tar.gz &&\
    export DOTNET_ROOT=/usr/bin/dotnet &&\
    export PATH=$PATH:/usr/bin/dotnet
}

function prepareWorkspace() {
    installPackages &&\
    installDotnet &&\
    configureRedis
}

echo "Preparing workspace..."
prepareWorkspace &> /dev/null
echo "Done!"