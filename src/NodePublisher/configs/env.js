module.exports = {
    serviceName: process.env.SERVICE_NAME || "NodePublisher",
    queueName: process.env.QUEUE_NAME || "healtcheck-queue",
    queueHost: process.env.QUEUE_HOST || "localhost",
    queuePort: process.env.QUEUE_PORT || "5672"
};