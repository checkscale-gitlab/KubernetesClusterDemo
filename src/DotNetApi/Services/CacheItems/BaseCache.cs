using System;
using Microsoft.Extensions.DependencyInjection;
using DotNetApi.Models.Caches;
using DotNetApi.Services.Caches;

namespace DotNetApi.Services.CacheItems
{
    internal class BaseCache<T> where T : new()
    {
        protected readonly IGetCacheService<T> GetCacheService;
        protected readonly IRefreshCacheService<T> RefreshCacheService;
        protected readonly IRemoveCacheService<T> RemoveCacheService;
        protected readonly ISetCacheService<T> SetCacheService;

        protected CacheItem<T> CacheItem;

        protected BaseCache(CacheKey key, IServiceProvider serviceProvider)
        {
            GetCacheService = serviceProvider.GetService<IGetCacheService<T>>();
            RefreshCacheService = serviceProvider.GetService<IRefreshCacheService<T>>();
            RemoveCacheService = serviceProvider.GetService<IRemoveCacheService<T>>();
            SetCacheService = serviceProvider.GetService<ISetCacheService<T>>();
            
            CacheItem = new CacheItem<T>(key);
            CacheItem = GetCacheService.Get(CacheItem);
        }
    }
}