using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using DotNetApi.Models.Caches;

namespace DotNetApi.Services.Caches
{
    internal class SetCacheService<T> : ISetCacheService<T> where T : new()
    {
        private readonly IDistributedCache _cache;
        private readonly IConvertObjectToCache<T> _converter;

        public SetCacheService(IDistributedCache cache, IConvertObjectToCache<T> converter)
        {
            _cache = cache;
            _converter = converter;
        }

        public void Set(CacheItem<T> cacheItem)
        {
            byte[] cacheValue = _converter.Convert(cacheItem.Value);
            _cache.Set(cacheItem.Key, cacheValue);
        }

        public async Task SetAsync(CacheItem<T> cacheItem)
        {
            byte[] cacheValue = _converter.Convert(cacheItem.Value);
            await _cache.SetAsync(cacheItem.Key, cacheValue);
        }
    }
}