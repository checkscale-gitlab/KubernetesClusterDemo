using System.Threading.Tasks;
using DotNetApi.Models.Caches;

namespace DotNetApi.Services.Caches
{
    public interface IRemoveCacheService<T> where T : new()
    {
        void Remove(CacheItem<T> cacheItem);
        Task RemoveAsync(CacheItem<T> cacheItem);
    }
}