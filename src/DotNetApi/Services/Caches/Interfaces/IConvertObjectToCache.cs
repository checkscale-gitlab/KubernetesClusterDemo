using System.Text;
using System.Text.Json;

namespace DotNetApi.Services.Caches
{
    public interface IConvertObjectToCache<T> where T : new()
    {
        internal byte[] Convert(T objectValue)
        {
            string objectValueJson = objectValue == null
                ? string.Empty
                : JsonSerializer.Serialize<T>(objectValue);

            return Encoding.ASCII.GetBytes(objectValueJson);
        }
    }

    internal class ConvertObjectToCache<T> : IConvertObjectToCache<T> where T : new()
    {
    }
}