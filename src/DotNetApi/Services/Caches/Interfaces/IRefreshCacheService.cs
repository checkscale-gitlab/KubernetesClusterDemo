using System.Threading.Tasks;
using DotNetApi.Models.Caches;

namespace DotNetApi.Services.Caches
{
    public interface IRefreshCacheService<T> where T : new()
    {
        void Refresh(CacheItem<T> cacheItem);
        Task RefreshAsync(CacheItem<T> cacheItem);
    }
}