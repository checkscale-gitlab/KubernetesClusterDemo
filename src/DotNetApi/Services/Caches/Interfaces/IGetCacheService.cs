using System.Threading.Tasks;
using DotNetApi.Models.Caches;

namespace DotNetApi.Services.Caches
{
    public interface IGetCacheService<T> where T : new()
    {
        CacheItem<T> Get(CacheItem<T> cacheItem);
        Task<CacheItem<T>> GetAsync(CacheItem<T> cacheItem);
    }
}