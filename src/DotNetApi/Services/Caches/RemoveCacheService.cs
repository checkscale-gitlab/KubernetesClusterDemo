using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using DotNetApi.Models.Caches;

namespace DotNetApi.Services.Caches
{
    internal class RemoveCacheService<T> : IRemoveCacheService<T> where T : new()
    {
        private readonly IDistributedCache _cache;

        public RemoveCacheService(IDistributedCache cache) => _cache = cache;

        public void Remove(CacheItem<T> cacheItem) => _cache.Remove(cacheItem.Key);

        public async Task RemoveAsync(CacheItem<T> cacheItem) => await _cache.RemoveAsync(cacheItem.Key);
    }
}