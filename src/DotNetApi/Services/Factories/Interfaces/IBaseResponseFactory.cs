using System.Threading.Tasks;
using DotNetApi.Models.Bases;

namespace DotNetApi.Services.Factories
{
    public interface IBaseResponseFactory
    {
        Task<BaseResponse> GetBaseResponse();
    }
}