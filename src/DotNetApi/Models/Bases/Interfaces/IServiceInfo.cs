using System;

namespace DotNetApi.Models.Bases
{
    public interface IServiceInfo
    {
        DateTime ServiceCreatedDateTime { get; }
        Guid ServiceId { get; }
        String ServiceName { get; }
    }
}