using System;
using System.Reflection;

namespace DotNetApi.Models.Bases
{
    public class ReadOnlyServiceInfo : IServiceInfo
    {
        private static readonly DateTime _serviceCreatedDateTime = DateTime.Now;
        private static readonly Guid _serviceId = Guid.NewGuid();
        private static readonly string _serviceName = Assembly.GetCallingAssembly().GetName().Name;

        public DateTime ServiceCreatedDateTime => _serviceCreatedDateTime;
        public Guid ServiceId => _serviceId;
        public String ServiceName => _serviceName;
    }
}