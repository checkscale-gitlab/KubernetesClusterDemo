using System;

namespace DotNetApi.Models.Bases
{
    internal record WriteOnlyServiceInfo(DateTime ServiceCreatedDateTime, Guid ServiceId, String ServiceName) : IServiceInfo;
}