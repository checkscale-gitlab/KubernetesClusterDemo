using System;

namespace DotNetApi.Models.Bases
{
    public class BaseResponse : ReadOnlyServiceInfo
    {
        public DateTime ResponseDateTime => DateTime.Now;
        public Guid ResponseId => Guid.NewGuid();
    }
}