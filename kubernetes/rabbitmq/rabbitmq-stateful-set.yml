apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: rabbitmq-stateful-set
  namespace: kubernetes-cluster-demo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: rabbitmq
  serviceName: rabbitmq-service
  template:
    metadata:
      annotations:
        scheduler.alpha.kubernetes.io/affinity: >
          {
            "podAntiAffinity": {
              "requiredDuringSchedulingIgnoredDuringExecution": [{
                "labelSelector": {
                  "matchExpressions": [{
                    "key": "app",
                    "operator": "In",
                    "values": ["rabbitmq"]
                  }]
                },
                "topologyKey": "kubernetes.io/hostname"
              }]
            }
          }
      labels:
        app: rabbitmq
        type: messages-broker
    spec:
      containers:
        - name: rabbitmq-container
          image: rabbitmq:alpine
          imagePullPolicy: Always
          env:
            - name: HOSTNAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: K8S_SERVICE_NAME
              value: "rabbitmq-service"
            - name: RABBITMQ_NODENAME
              value: "rabbitmq-service@$(HOSTNAME).rabbitmq-service.$(NAMESPACE).svc.cluster.local"
            - name: RABBITMQ_USE_LONGNAME
              value: "true"
          ports:
            - name: amqp
              protocol: TCP
              containerPort: 5672
          volumeMounts:
            - name: rabbitmq-config-map
              mountPath: /etc/rabbitmq
            - name: rabbitmq-persistent-volume-claim
              mountPath: /var/lib/rabbitmq/mnesia
          livenessProbe:
            exec:
              command: ["rabbitmqctl", "status"]
            initialDelaySeconds: 60
            periodSeconds: 10
            timeoutSeconds: 10
          readinessProbe:
            exec:
              command: ["rabbitmqctl", "status"]
            initialDelaySeconds: 10
            periodSeconds: 10
            timeoutSeconds: 10
      serviceAccountName: rabbitmq-service-account
      terminationGracePeriodSeconds: 10
      volumes:
        - name: rabbitmq-config-map
          configMap:
            name: rabbitmq-config-map
            items:
              - key: enabled_plugins
                path: enabled_plugins
              - key: rabbitmq.conf
                path: rabbitmq.conf
        - name: rabbitmq-persistent-volume-claim
          persistentVolumeClaim:
            claimName: rabbitmq-persistent-volume-claim